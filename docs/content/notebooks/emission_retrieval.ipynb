{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Retrievals: Emission Spectra Retrieval\n",
    "\n",
    "This retrieval is based on Mollière+20 for HR8799e, and shows a more realistic example of how to set up a retrieval.\n",
    "\n",
    "For this example, the module `poor_mans_noneq_chemistry` is used to solve disequilibrium chemistry, and the `cloud_cond` module is used for condensation. You can import other models from `models.py`, or write your own function, ensuring that it takes in the same set of inputs and outputs, which are documented in models, see the [API documentation](../../autoapi/petitRADTRANS/retrieval/models/index.html).\n",
    "\n",
    "This model uses a simple adaptive mesh refinement (AMR) algorithm to improve the pressure resolution around the location of the cloud bases.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting started\n",
    "\n",
    "You should already have an installation of pRT on your machine, please see the [installation manual](../installation.html) if you have not installed it yet. \n",
    "[pyMultiNest](https://github.com/JohannesBuchner/PyMultiNest) is also required. [Ultranest](https://johannesbuchner.github.io/UltraNest/index.html) is required if you want to use that as your sampler rather than pyMultiNest. See the Ultranest documentation for why you might want to choose this method.\n",
    "Using nested sampling rather than MCMC is faster, handles multimodal cases better, and directly provides the Bayesian evidence, which allows for easier model comparison. \n",
    "\n",
    "\n",
    "In this tutorial, we will outline the process of setting up a RetrievalConfig object, which is the class used to set up a pRT retrieval.\n",
    "The basic process will always be to set up the configuration, and then pass it to the Retrieval class to run the retrieval using pyMultiNest.\n",
    "Several standard plotting outputs will also be produced by the retrieval class.\n",
    "Most of the classes and functions used in this tutorial have more advanced features than what will be explained here, so it's highly recommended to take a look at the code and API documentation. \n",
    "There should be enough flexibility built in to cover most typical retrieval studies, but if you have feature requests please get in touch, or open an issue on [gitlab](https://gitlab.com/mauricemolli/petitRADTRANS.git).\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's start by importing everything we need.\n",
    "import os\n",
    "# To not have numpy start parallelizing on its own\n",
    "os.environ[\"OMP_NUM_THREADS\"] = \"1\"\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from petitRADTRANS import Radtrans\n",
    "from petitRADTRANS import nat_cst as nc\n",
    "\n",
    "# Import the class used to set up the retrieval.\n",
    "from petitRADTRANS.retrieval import Retrieval,RetrievalConfig\n",
    "# Import Prior functions, if necessary.\n",
    "from petitRADTRANS.retrieval.util import gaussian_prior\n",
    "# Import atmospheric model function\n",
    "from petitRADTRANS.retrieval.models import emission_model_diseq"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "##########################\n",
    "# Define the pRT run setup\n",
    "##########################\n",
    "\n",
    "# retrieve.py expects this object to be called RunDefinition.\n",
    "RunDefinition = RetrievalConfig(retrieval_name = \"HR8799e_example\", # Give a useful name for your retrieval \n",
    "                                run_mode = \"retrieval\", # 'retrieval' to run, or 'evaluate' to make plots\n",
    "                                AMR = True,             # Adaptive mesh refinement, slower if True\n",
    "                                scattering = True)      # Add scattering for emission spectra clouds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this example we include the GRAVITY data as published in Mollière+20. To reproduce the published results, please also include the archival SPHERE and GPI data from [Zurlo et al. (2015)](https://www.aanda.org/articles/aa/full_html/2016/03/aa26835-15/aa26835-15.html) and [Greenbaum et al. (2016)](https://iopscience.iop.org/article/10.3847/1538-3881/aabcb8)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "##################\n",
    "# Read in Data\n",
    "##################\n",
    "\n",
    "import petitRADTRANS # need to get the name for the example data\n",
    "path_to_data = petitRADTRANS.__file__.split('__init__.py')[0] # Default location for the example\n",
    "\n",
    "RunDefinition.add_data('GRAVITY',\n",
    "                       f\"{path_to_data}retrieval/examples/emission/observations/HR8799e_Spectra.fits\",\n",
    "                       data_resolution = 500,\n",
    "                       model_resolution = 1000,\n",
    "                       model_generating_function = emission_model_diseq)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Photometric data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to add photometry, we can do that as well! The photometry file should have the format:\n",
    "\n",
    "`# Name, lower wavelength edge [um], upper wavelength edge [um], flux density [W/m2/micron], flux error [W/m2/micron]`\n",
    "\n",
    "You are required to provide a model function for calculating the spectrum, as with spectral data, but also a photometric transformation function, which is used to convert the model spectrum into synthetic photometry. This would typically make use of the transmission function for a particular filter. We recommend the use of the `species` package (https://species.readthedocs.io/), in particular the `SyntheticPhotometry` module to provide these functions. If no function is provided, the RetrievalConfig will attempt to import `species` to use this module, using the `name` provided as the filter name.\n",
    "\n",
    "If you are using transmission spectra, your photometric transformation function should model the difference between the clear and occulted stellar spectrum, returning the difference in (planet radius/stellar radius)^2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "RunDefinition.add_photometry(path_to_data + 'retrieval/examples/emission/observations/hr8799e_photometry.txt',\n",
    "                             emission_model_diseq,\n",
    "                             model_resolution = 40)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters and Priors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we add all of the parameters used in the retrieval. \n",
    "Each parameter must be given a name, which matches the name used in the model function.\n",
    "Parameters can be set to fixed or free. Fixed parameters must be given a value, while free parameters are given a function that transforms the unit hypercube generated by multinest into physical prior space. Various prior functions are stored in util, see the [API documentation](../../autoapi/petitRADTRANS/retrieval/util/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#################################################\n",
    "# Add parameters, and priors for free parameters.\n",
    "#################################################\n",
    "\n",
    "# This run uses the model of Molliere (2020) for HR8799e\n",
    "# The lambda function provide uniform priors.\n",
    "\n",
    "# Distance to the planet in cm\n",
    "RunDefinition.add_parameter(name = 'D_pl', free = False, value = 41.2925*nc.pc)\n",
    "\n",
    "# Log of the surface gravity in cgs units.\n",
    "RunDefinition.add_parameter('log_g',True, \n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 2.+3.5*x)\n",
    "\n",
    "# Planet radius in cm\n",
    "RunDefinition.add_parameter('R_pl', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : ( 0.7+1.3*x)*nc.r_jup_mean)\n",
    "\n",
    "# Temperature in Kelvin\n",
    "RunDefinition.add_parameter('T_int', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 300.+2000.*x,\n",
    "                            value=0.0)\n",
    "\n",
    "# Spline temperature structure parameters. T1 < T2 < T3\n",
    "# As these priors depend on each other, they are implemented in the model function\n",
    "RunDefinition.add_parameter('T3', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : x,\n",
    "                            value=0.0)\n",
    "RunDefinition.add_parameter('T2', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : x,\n",
    "                            value=0.0)\n",
    "RunDefinition.add_parameter('T1', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : x)\n",
    "# Optical depth model\n",
    "# power law index in tau = delta * press_cgs**alpha\n",
    "RunDefinition.add_parameter('alpha', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x :1.0+x)\n",
    "# proportionality factor in tau = delta * press_cgs**alpha\n",
    "RunDefinition.add_parameter('log_delta', True, \\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : x) \n",
    "# Chemistry\n",
    "# A 'free retrieval' would have each line species as a parameter\n",
    "# Using a (dis)equilibrium model, we only supply bulk parameters.\n",
    "# Carbon quench pressure\n",
    "RunDefinition.add_parameter('log_pquench',True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : -6.0+9.0*x)\n",
    "# Metallicity\n",
    "RunDefinition.add_parameter('Fe/H',True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : -1.5+3.0*x)\n",
    "# C/O ratio\n",
    "RunDefinition.add_parameter('C/O',True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 0.1+1.5*x)\n",
    "# Clouds\n",
    "# Based on an Ackermann-Marley (2001) cloud model\n",
    "# Width of particle size distribution\n",
    "RunDefinition.add_parameter('sigma_lnorm', True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 1.05 + 1.95*x) \n",
    "# Vertical mixing parameters\n",
    "RunDefinition.add_parameter('log_kzz',True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 5.0+8.*x) \n",
    "# Sedimentation parameter\n",
    "RunDefinition.add_parameter('fsed',True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 1.0 + 10.0*x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#######################################################\n",
    "# Define species to be included as absorbers\n",
    "#######################################################\n",
    "RunDefinition.set_rayleigh_species(['H2', 'He'])\n",
    "RunDefinition.set_continuum_opacities(['H2-H2', 'H2-He'])\n",
    "RunDefinition.set_line_species(['CH4',\n",
    "                                'H2O_HITEMP',\n",
    "                                'CO2',\n",
    "                                'HCN',\n",
    "                                'CO_all_iso_HITEMP',\n",
    "                                'FeH', \n",
    "                                'H2S',\n",
    "                                'NH3',\n",
    "                                'PH3',\n",
    "                                'Na_allard',\n",
    "                                'K_allard'],\n",
    "                                eq = True)\n",
    "RunDefinition.add_cloud_species('Fe(c)_cd',eq = True,scaling_factor=(-2.5,4.5))\n",
    "RunDefinition.add_cloud_species('MgSiO3(c)_cd',eq = True,scaling_factor=(-2.5,4.5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Before we run the retrieval, let's set up plotting.\n",
    "\n",
    "##################################################################\n",
    "# Define what to put into corner plot if run_mode == 'evaluate'\n",
    "##################################################################\n",
    "RunDefinition.parameters['R_pl'].plot_in_corner = True\n",
    "RunDefinition.parameters['R_pl'].corner_label = r'$R_{\\rm P}$ ($\\rm R_{Jup}$)'\n",
    "RunDefinition.parameters['R_pl'].corner_transform = lambda x : x/nc.r_jup_mean\n",
    "RunDefinition.parameters['log_g'].plot_in_corner = True\n",
    "RunDefinition.parameters['log_g'].corner_ranges = [2., 5.]\n",
    "RunDefinition.parameters['log_g'].corner_label = \"log g\"\n",
    "RunDefinition.parameters['fsed'].plot_in_corner = True\n",
    "RunDefinition.parameters['log_kzz'].plot_in_corner = True\n",
    "RunDefinition.parameters['log_kzz'].corner_label = \"log Kzz\"\n",
    "RunDefinition.parameters['C/O'].plot_in_corner = True\n",
    "RunDefinition.parameters['Fe/H'].plot_in_corner = True\n",
    "RunDefinition.parameters['log_pquench'].plot_in_corner = True\n",
    "RunDefinition.parameters['log_pquench'].corner_label = \"log pquench\"\n",
    "\n",
    "for spec in RunDefinition.cloud_species:\n",
    "    cname = spec.split('_')[0]\n",
    "    RunDefinition.parameters['eq_scaling_'+cname].plot_in_corner = True\n",
    "    RunDefinition.parameters['eq_scaling_'+cname].corner_label = cname\n",
    "\n",
    "##################################################################\n",
    "# Define axis properties of spectral plot if run_mode == 'evaluate'\n",
    "##################################################################\n",
    "RunDefinition.plot_kwargs[\"spec_xlabel\"] = 'Wavelength [micron]'\n",
    "RunDefinition.plot_kwargs[\"spec_ylabel\"] = \"Flux [W/m2/micron]\"\n",
    "\n",
    "RunDefinition.plot_kwargs[\"y_axis_scaling\"] = 1.0\n",
    "RunDefinition.plot_kwargs[\"xscale\"] = 'linear'\n",
    "RunDefinition.plot_kwargs[\"yscale\"] = 'linear'\n",
    "RunDefinition.plot_kwargs[\"resolution\"] = 1000.# Maximum resolution, will bin any higher resolution data to this resolution\n",
    "RunDefinition.plot_kwargs[\"nsample\"] = 100 # If we want a plot with many sampled spectra\n",
    "\n",
    "##################################################################\n",
    "# Define from which observation object to take P-T\n",
    "# in evaluation mode (if run_mode == 'evaluate'),\n",
    "# add PT-envelope plotting options\n",
    "##################################################################\n",
    "RunDefinition.plot_kwargs[\"take_PTs_from\"] = 'GRAVITY'\n",
    "RunDefinition.plot_kwargs[\"temp_limits\"] = [150, 3000]\n",
    "RunDefinition.plot_kwargs[\"press_limits\"] = [1e2, 1e-5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's run the retrieval.\n",
    "Note that for this setup we're using 4000 live points, and retrieving an emission spectrum on moderate resolution data. \n",
    "This will take some time: probably a day or so running on a cluster, **so don't try to run this on your laptop!**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "retrieval = Retrieval(RunDefinition,\n",
    "                      output_dir = \"\",\n",
    "                      use_MPI=True,\n",
    "                      sample_spec = False)\n",
    "retrieval.run(n_live_points = 4000,\n",
    "      sampling_efficiency=0.05,\n",
    "      const_efficiency_mode=True,\n",
    "      resume = True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "retrieval.plot_all(contribution = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Contact**\n",
    "\n",
    "If you need any additional help, don't hesitate to contact [Evert Nasedkin](mailto:nasedkinevert@gmail.com?subject=[petitRADTRANS]%20Retrievals)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.3"
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "metadata": {
     "collapsed": false
    },
    "source": []
   }
  },
  "vscode": {
   "interpreter": {
    "hash": "207b56136aaee7ec9eed3aa3bd9bf42bb51cfe74d6567821df16aa3ffeea32bf"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
